# Transaction History Management

## Setup

The first thing to do is to clone the repository:

```sh
$ git clone https://gitlab.com/mobidev86/transactionanalytics.git
$ cd transactionanalytics
```

Create a virtual environment to install dependencies in and activate it:

```sh
$ virtualenv2 --no-site-packages historyenv
$ source historyenv/bin/activate
```

Then install the dependencies:

```sh
(historyenv)$ pip install -r requirements.txt
```
Note the `(historyenv)` in front of the prompt. This indicates that this terminal
session operates in a virtual environment set up by `virtualenv2`.

Once `pip` has finished downloading the dependencies:
```sh
(historyenv)$ cd project
(historyenv)$ python manage.py migrate
(historyenv)$ python manage.py makemigrations
(historyenv)$ python manage.py createsuperuser
(historyenv)$ python manage.py runserver
```
And navigate to `http://127.0.0.1:8000/`
