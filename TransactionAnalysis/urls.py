"""
    TransactionAnalysis URL Configuration
"""
from django.contrib import admin
from django.urls import path, include, re_path as url

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r"", include(("accountmgmt.urls", "accountmgmt"), namespace="accountmgmt")),
    url(r"^", include(("txnanalytics.urls", "txnanalytics"), namespace="txnanalytics")),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
