$(document).ready(function(event){
    window.onload = function() {

        var pieCharRawData = {{pieCharData|safe}};

        var pieDataPoints = [];

        for(var piedt=0; piedt<pieCharRawData.length; piedt++){
            debugger
            var curr_data = pieCharRawData[piedt];
            var pieSliceData = {
                y: curr_data.total,
                label: curr_data.title.split("__").join(" "),
                credit: curr_data.credit,
                debit: curr_data.debit
            };
            pieDataPoints.push(pieSliceData);
        }

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            title: {
                text: "Transaction Analysis - MobiDev"
            },
            data: [{
                type: "pie",
                startAngle: 270,
                // yValueFormatString: "Credit: 200, Debit: 300",
                indexLabel: "{label} {credit} {debit}",
                dataPoints: pieDataPoints
            }]
        });

        chart.render();
    }
})