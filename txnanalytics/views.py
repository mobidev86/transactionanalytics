"""
    - Used to manage Transaction History Analytics using PieChart
"""
import csv

from datetime import datetime, timedelta

from django.db import transaction

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import JsonResponse

from django.shortcuts import render, redirect
from django.views.generic import ListView, View, TemplateView

from txnanalytics.models import TransactionHistory

def getTwelveMonthDate():
    """
        - Used to return current & 12 month older date for filtering purpose
    """

    current_date = datetime.today()
    months_ago = 12
    twelve_month_previous_date = current_date - timedelta(days=(months_ago * 365 / 12))
    return current_date, twelve_month_previous_date

def commonPieChartDataFilter(allTxnObjs):
    """
        - Create common function for rendering Pie-Chart data
    """

    pieChartData = []

    for txn in allTxnObjs:
        default_pie_slice = {"credit": 0, "debit": 0, "total": 0, "title": "", "month_name": "" }
        month = txn.txn_date.strftime("%b")
        year = txn.txn_date.strftime("%Y")
        pie_slice_title = month + "__" + year

        piesliceexist = any(pie_slice_title in pieexist.values() for pieexist in pieChartData)

        if piesliceexist:
            exist_pie_data = next((item for item in pieChartData if item['title'] == pie_slice_title), None)

            if txn.txn_category == "credit":
                exist_pie_data["credit"] += txn.txn_amount
            else:
                exist_pie_data["debit"] += txn.txn_amount

            exist_pie_data["total"] = exist_pie_data["credit"] + exist_pie_data["debit"]

        else:

            if txn.txn_category == "credit":
                default_pie_slice["credit"] += txn.txn_amount
            else:
                default_pie_slice["debit"] += txn.txn_amount

            default_pie_slice["total"] = default_pie_slice["credit"] + default_pie_slice["debit"]
            default_pie_slice["title"] = pie_slice_title
            default_pie_slice["month_name"] = month

            pieChartData.append(default_pie_slice)

    pieChartData = sorted(pieChartData, key=lambda d: datetime.strptime(d['title'],'%b__%Y'))

    return pieChartData


class PieChartFilterByTypeView(LoginRequiredMixin, View):
    """
        - Used to render Pie-Chart data
    """

    def get(self, *args, **kwargs):
        """
            - Used to render Pie-Chart data
        """
        txn_type = "alltxn"
        if self.request.GET and "txn_type" in self.request.GET and self.request.GET.get("txn_type"):
            txn_type = self.request.GET.get("txn_type")

        current_date, twelve_month_previous_date = getTwelveMonthDate()

        txnfilter = {
            "txn_date__gte": twelve_month_previous_date,
            "txn_date__lte": current_date,
        }

        if txn_type != "alltxn":
            txnfilter.update({"txn_category": txn_type})

        allTxnObjs = TransactionHistory.objects.filter(**txnfilter)

        pieChartData = commonPieChartDataFilter(allTxnObjs)

        return JsonResponse({
            "pieChartData": pieChartData,
            "filterTxnType": txn_type
        })


class TransactionHistoryListView(LoginRequiredMixin, ListView):
    """
        - Used to render Transaction data in List-View
    """

    model = TransactionHistory
    paginate_by = 15
    template_name = "home.html"

    def get_queryset(self):
        """
            - Used to return QuerySets
        """
        queryset = super().get_queryset()
        return queryset

    def get_context_data(self, *args, object_list=None, **kwargs):
        """
            - Used to manage queryset with custom context for rendering purpose
        """

        context = super().get_context_data(*args, **kwargs)

        current_date, twelve_month_previous_date = getTwelveMonthDate()

        allTxnObjs = TransactionHistory.objects.filter(
            txn_date__gte=twelve_month_previous_date,
            txn_date__lte=current_date
        )

        pieChartData = commonPieChartDataFilter(allTxnObjs)

        context["allTxnObjs"] = allTxnObjs
        context["pieChartData"] = pieChartData

        return context


class UploadCsvView(TemplateView):
    """
        - Upload CSV-View to manage CSV Upload section
    """

    template_name = 'transaction/upload_csv_view.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context

    def post(self, args, *kwargs):
        all_files = self.request.FILES.getlist('files', None)
        with transaction.atomic():
            all_errors = {}

            for file in all_files:
                if not file.name.endswith('.csv'):
                    messages.error(self.request, "Please Enter Valid File. uploaded file format is not CSV.")
                    return redirect('txnanalytics:upload_csv')
                decoded_file = file.read().decode('utf-8').splitlines()
                reader = csv.DictReader(decoded_file)
                reader.fieldnames = [h.lower() for h in reader.fieldnames]
                errors = []
                count = 1
                for row in reader:
                    is_error = False
                    count += 1
                    try:
                        id = int(row['id'])
                    except Exception as e:
                        print(e)
                        errors.append({count: "Invalid Id - " + row['id']})
                        is_error = True
                    try:
                        amount = float(row['amount'])
                    except Exception as e:
                        print(e)
                        errors.append({count: "Invalid Amount -" + row['amount']})
                        is_error = True
                    try:
                        date = datetime.strptime(row['date'], "%Y-%m-%d %H:%M:%S")
                    except Exception as e:
                        print(e)
                        errors.append({count: "Invalid DateTime format (YYYY-mm-dd HH:MM:SS format).-"+row['date']})
                        is_error = True

                    category = row['category']
                    if category.lower() not in ['credit', 'debit']:
                        errors.append({count: "Invalid Category -" +category})
                        is_error = True

                    if is_error:
                        continue
                    obj, created = TransactionHistory.objects.get_or_create(txn_id=id, defaults={
                        'txn_amount': amount, 'txn_category': category, 'txn_date': date
                    })

                    if not created:
                        if date > obj.txn_date.replace(tzinfo=None):
                            obj.txn_amount = amount
                            obj.txn_category = category
                            obj.txn_date = date
                            obj.save()
                if errors:
                    all_errors[file.name] = errors
                    transaction.set_rollback(True)
        if all_errors:
            messages.error(self.request, "CSV DATA not Saved Please fix error first and upload again.")
            return render(self.request, 'transaction/upload_csv_view.html', {'errors': all_errors})
        messages.success(self.request, "CSV DATA Saved successfully.")
        return redirect('txnanalytics:txn_dashboard')
