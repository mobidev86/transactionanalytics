"""
    - Transaction Analytics History Datatable Management
"""
from ajax_datatable.views import AjaxDatatableView
from .models import TransactionHistory


class TransactionHistoryAjaxDatatableView(AjaxDatatableView):
    """
        - Transaction Analytics History Datatable Management
    """

    model = TransactionHistory
    title = 'TransactionHistory'
    initial_order = [["txn_id", "asc"],]
    length_menu = [[10, 50, 100, -1], [10, 50, 100, "All"]]
    show_column_filters = False

    column_defs = [
        {'name': 'txn_id', 'title': 'Transaction Id', 'visible': True, },
        {'name': 'txn_amount', 'title': 'Amount', 'visible': True, },
        {'name': 'txn_category', 'title': 'Category', 'visible': True, },
        {'name': 'txn_date', 'visible': True},
    ]

TransactionHistoryView = TransactionHistoryAjaxDatatableView.as_view()
