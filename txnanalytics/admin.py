"""
    - Transaction Analytics Admin for Registrtaion
"""

from django.contrib import admin

from txnanalytics.models import TransactionHistory


class TransactionHistoryAdmin(admin.ModelAdmin):
    """
        - Transaction Analytics define Modal Admin
    """

    list_display = ["txn_id", "txn_amount", "txn_category", "txn_date", "is_active"]


admin.site.register(TransactionHistory, TransactionHistoryAdmin)
