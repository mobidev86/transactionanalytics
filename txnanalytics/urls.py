"""
    - Transaction Analytics URLs
"""

from django.urls import path

from txnanalytics.txn_history_view import TransactionHistoryView
from txnanalytics.views import (
    TransactionHistoryListView,
    UploadCsvView,
    PieChartFilterByTypeView
)

# app_name = "txnanalytics"


urlpatterns = [
    path("", TransactionHistoryListView.as_view(), name="txn_dashboard"),

    path('upload/csv/', UploadCsvView.as_view(), name='upload_csv'),
    path('ajax_datatable/transaction-history/list/',
        TransactionHistoryView, name="ajax_datatable_permissions"),

    path("filter-by/txn-category/",
        PieChartFilterByTypeView.as_view(), name="filter_by_txn_category"),
]
