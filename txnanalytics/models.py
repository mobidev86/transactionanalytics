"""
    - Used to manage Dashboard analytics according to Transaction History or Records
"""
from django.db import models

TRANSACTION_CATEGORY = (
    ("credit", "Credit"),
    ("debit", "Debit"),
)


class TransactionHistory(models.Model):
    """
        - Used to manage Transaction related history on DB fields
        - Show or Manage Dashboard analytics
    """
    txn_id = models.CharField("Transaction ID",
        max_length=100, help_text="Used to describe Transaction ID", unique=True)
    txn_amount = models.FloatField("Amount",
        default=0, help_text="Used to show Transaction Amount")
    txn_category = models.CharField("Transaction Category",
        choices=TRANSACTION_CATEGORY, max_length=9)
    txn_date = models.DateTimeField("Transaction Date",
        help_text="Used to show Transaction Date")

    is_active = models.BooleanField("Is Active", default=True)

    def __str__(self):
        return str(self.txn_id)
