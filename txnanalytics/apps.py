"""
    - Transaction Analytics App Config
"""

from django.apps import AppConfig


class TxnanalyticsConfig(AppConfig):
    """
        - Transaction Analytics App Config
    """

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'txnanalytics'
