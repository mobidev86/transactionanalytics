"""
    Used to manage Users Account
"""

from django.db import models
from django.contrib.auth.models import User


GENDER_CHOICE = (
    ('male', 'male'),
    ('female', 'female'),
)


class UserProfile(models.Model):
    """
        # Used to define user related fields

        - User: lined with default user
        - Username: username of registered user
        - First Name: First name of user
        - Last Name: Last name of user
        - Email: Email address of the user
        - Password: User Password
        - Profile Pic: Profile picture of the user
        - Phone No: Phone Number
        - Gender: Users gender
        - DOB: Date of Birth of the user
        - is_active: Used to check is user is active in syste or not?
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)

    username = models.CharField("Username", max_length=100)

    first_name = models.CharField("First Name", max_length=64)
    last_name = models.CharField("Last Name", max_length=64)

    email = models.CharField("Email", max_length=80, unique=True)
    password = models.CharField("Password", max_length=100)

    phone_no = models.CharField("Phone Number", max_length=50)

    is_active = models.BooleanField("Is Active", default=False)

    def __str__(self):
        return str(self.username)
