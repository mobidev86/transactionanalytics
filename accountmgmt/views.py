"""
    Used to manage views related to Account or User
"""
from django.shortcuts import redirect
from django.views.generic import CreateView, TemplateView, View, FormView
from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.models import User

from .forms import RegistrationForm, LoginForm


class RegistrationView(CreateView):
    """
        Used to manager User Registrataion view
    """

    template_name = 'account/register.html'
    success_url = '/'
    form_class = RegistrationForm


    def form_valid(self, form):
        super().form_valid(form)

        user, created = User.objects.get_or_create(
            username=self.object.username,
            first_name=self.object.first_name,
            last_name=self.object.last_name,
            email=self.object.email
        )
        print(created)
        user.set_password(self.request.POST.get('password'))
        user.is_active = True
        user.save()
        self.object.user = user
        self.object.save()

        messages.success(self.request,
            "User register successfully. Please varify your email for login"
        )
        return redirect('accountmgmt:login')


class LoginView(FormView):
    """
        Used to manage User Login view
    """

    template_name = "account/login.html"
    form_class = LoginForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        messages.success(self.request, "User logged in successfully.")
        return redirect("txnanalytics:txn_dashboard")


class LogoutView(FormView):
    """
        Used to manage User Logout View
    """

    def get(self, *args, **kwargs):
        logout(self.request)
        messages.success(self.request, "User logged out successfully.")
        return redirect('/')
