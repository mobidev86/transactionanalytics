"""
    Used to manage all the URL's related to User Account
"""

from django.urls import path
# from accountmgmt.views import HomeView, RegistrationView, LoginView, LogoutView
from accountmgmt.views import RegistrationView, LoginView, LogoutView

urlpatterns = [
    # path('/', HomeView.as_view(), name="home"),
    path('register/', RegistrationView.as_view(), name="signup"),
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(), name="logout"),
    # path('emailVerification/<uidb64>/<token>', activate, name='activate')
]
