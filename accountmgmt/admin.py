"""
    Account Management Docs
"""

from django.contrib import admin
from .models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    """
        User Profile Admin
    """

    list_display = [
        "user", "username", "first_name", "last_name",
        "email", "phone_no", "is_active"
    ]


admin.site.register(UserProfile, UserProfileAdmin)
